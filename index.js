// Add css in console.log
let css = "background-color: salmon; padding:5px; font-weight: bold;"

// Declare variables with different data types
let firstName = 'Earl'
let lastName = 'Diaz'
let age = 23
let hobbies = ['Cycling', 'Travel', 'Food']
let workAddress = {
    houseNumber: 50,
    street: 'Nicanor Ramirez',
    city: 'Quezon City',
    state: 'NCR'
}

// Declared a function that takes values pertaining to a persons details
function printUserInfo(first_name, last_name, age, hobbies, work_address)
{
    // Display info in console
    console.log(`First Name: ${first_name}`)
    console.log(`Last Name: ${last_name}`)
    console.log(`Age: ${age}`)
    console.log(`Hobbies: ${hobbies}`)
    console.log(`Work Address: ${JSON.stringify(work_address)}`)
    console.log(`${first_name} ${last_name} is ${age} years of age.`)
    console.log('%c This was printed inside of the function', css)
    console.log(hobbies)
    console.log('%c This was printed inside of the function', css)
    console.log(JSON.stringify(work_address))
}

// Invoke function and put declared variables in parameters
printUserInfo(
    firstName,
    lastName,
    age,
    hobbies,
    workAddress
)

// Declared a function which returns a value and assigns that value to the is_married variable
function returnFunction()
{
    return true
}

let is_married = returnFunction()

console.log(`The value of isMarried is: ${is_married}`)